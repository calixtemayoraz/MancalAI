from abc import ABC, abstractmethod
from contextlib import redirect_stderr
from typing import Optional
import random
import numpy as np
from enum import Enum, auto


class Well(ABC):
    """
    Base class of a Mancala Well

    Parameters
    ----------
    owner : Player
    num_stones: int
        if we need to initialize this well with a given number of stones to start

    Attributes
    ----------
    owner : Player
        the owner of the well
    num_stones : int
        the number of stones in this well
    right: PlayableWell|HomeWell
        well to the right of this well. Can be a playable well, a home well, or an opponent's playable well
    left: PlayableWell|Homewell
        well to the left of this well. Either this player's playable well, or the opponent's home well

    is_home: bool
        whether this is a home well.
    """

    def __init__(self, owner: 'Player', num_stones: int = 0):
        self.owner = owner
        self.num_stones = num_stones
        self._right: 'Optional[PlayableWell|HomeWell]' = None  # all wells know about the well to their right
        self._left: 'Optional[PlayableWell|HomeWell]' = None

    @property
    def left(self) -> 'PlayableWell|HomeWell':
        return self._left

    @left.setter
    def left(self, left: 'Well'):
        self._left = left

    @property
    def right(self) -> 'PlayableWell|HomeWell':
        return self._right

    @right.setter
    def right(self, right: 'Well'):
        self._right = right

    @property
    @abstractmethod
    def is_home(self) -> bool:
        pass

    def add_stones(self, num_stones=1):
        self.num_stones += num_stones

    def __repr__(self):
        return f"{self.owner.name} - {self.num_stones}"


class HomeWell(Well):
    """
    The home well.
    """

    @property
    def is_home(self) -> bool:
        return True


class PlayableWell(Well):
    """
    The playable well, from which the player can play

    Attributes
    ----------
    opposite : PlayableWell
        the opponent's well directly opposite this one.

    is_playable : bool
        whether this well is playable, i.e. it has stones inside

    position: int
        this well's position (0-5) going left-to-right.
    """

    def __init__(self, owner: 'Player', position: int, num_stones: int = 4):
        super(PlayableWell, self).__init__(owner, num_stones)
        self.position = position
        self._opposite: 'Optional[PlayableWell]' = None

    @property
    def opposite(self) -> 'PlayableWell':
        return self._opposite

    @opposite.setter
    def opposite(self, opposite: 'PlayableWell'):
        self._opposite = opposite

    @property
    def is_home(self) -> bool:
        return False

    @property
    def is_playable(self) -> bool:
        return self.num_stones > 0

    def pick_stones(self) -> int:
        """
        Sets this well's stone count to zero, and returns the number of stones there were before.

        Returns
        -------
        int
            the number of stones picked from this well
        """
        in_hand = self.num_stones
        self.num_stones = 0
        return in_hand

    def __repr__(self):
        return f"{self.owner.name}[{self.position}] - {self.num_stones}"


class TurnOutcome(Enum):
    """
    Enum class for the possible outcomes of a turn:
        - NONE -> nothing happens, turn is over
        - CAPTURE -> an opponent's piece was captured, turn is over
        - PLAY_AGAIN -> we finished in our home well, play again
    """
    NONE = auto()
    CAPTURE = auto()
    PLAY_AGAIN = auto()


class PlayerAlgorithm(Enum):
    """
    Enum class for the possible algorithms a player can use

    - HUMAN -> asks the human for input
    - RANDOM -> plays randomly
    - MINIMAX -> uses the minimax algorithm
    """
    HUMAN = auto()
    RANDOM = auto()
    MINIMAX = auto()


class Player:
    """
    Player class which plays the game

    Attributes
    ----------
    name: str
        the player's name
    opponent: Player
        the opponent
    home_well: HomeWell
        the player's home well
    playable_wells: list[PlayableWell]
        the 6 playable wells
    available_wells: list[PlayableWell]
        the playable wells that have stones inside
    algorithm: PlayerAlgorithm
        which algorithm the player is using

    """

    def __init__(
            self,
            name: str,
            minimax_depth: int = 1,
            init_stones: np.ndarray = np.full((6,), 4),
            init_home: int = 0,
            algorithm: PlayerAlgorithm = PlayerAlgorithm.RANDOM,
            verbose: bool = True
    ):
        self.__name = name
        self.minimax_depth = minimax_depth
        self._algorithm = algorithm
        self.opponent: 'Optional[Player]' = None
        self.verbose = verbose
        # set home_well
        self.home_well = HomeWell(self, num_stones=init_home)
        # set 6 playable_wells
        self.playable_wells = [
            PlayableWell(self, position=i, num_stones=stones)
            for i, stones in enumerate(init_stones)
        ]
        # set the neighbors for playable wells and home well
        for i, well in enumerate(self.playable_wells):
            # unless we are at the very left, set the left well to our neighbor (opponent's last well otherwise)
            if i > 0:
                well.left = self.playable_wells[i - 1]
            # unless we are to the very right, set this well's right to neighbor (home_well otherwise)
            if i < 5:
                well.right = self.playable_wells[i + 1]
            else:
                well.right = self.home_well
        # set the home well's left to last playable well
        self.home_well.left = self.playable_wells[5]

    @property
    def name(self):
        return self.__name

    @property
    def algorithm(self):
        return self._algorithm

    def set_opponent(self, opponent: 'Player'):
        """
        Sets the opponent, opposing wells, home well's right, and leftmost playable well's left.

        Parameters
        ----------
        opponent: Player
            the opponent
        """
        # set the opposite well for the playable wells
        for i, well in enumerate(self.playable_wells):
            well.opposite = opponent.playable_wells[5 - i]
        # set the left well for the leftmost well, which is the opponent's home well
        self.playable_wells[0].left = opponent.playable_wells[5]

        # set the next well for our home well, which is the opponent's first playable well
        self.home_well.right = opponent.playable_wells[0]

        # set the opponent
        self.opponent = opponent

    def do_play(self, well: PlayableWell) -> 'tuple[TurnOutcome, int]':
        """
        Mechanical method which distributes the stones from the given to the next wells.

        Returns
        -------
        TurnOutcome:
            the outcome of this turn
        int:
            the stones gained in the home well from this play
        """

        # pick the stones from this well...
        stones_in_hand = well.pick_stones()
        next_well: 'Well' = well.right
        score_before_play = self.home_well.num_stones
        # place anticlockwise in the next wells until we run out of stones...
        while True:
            next_well.add_stones()
            stones_in_hand -= 1

            if stones_in_hand > 0:
                next_well = next_well.right
                # we skip our opponent's home well
                if next_well.is_home and next_well.owner.name != self.name:
                    next_well = next_well.right
            else:
                last_played_well = next_well
                break
        # determine the outcome of the play
        if last_played_well.is_home:
            # we can play again!
            return TurnOutcome.PLAY_AGAIN, self.home_well.num_stones - score_before_play
        elif last_played_well.num_stones == 1 and last_played_well.owner.name == self.name:
            # capture
            self.home_well.add_stones(last_played_well.pick_stones())
            self.home_well.add_stones(last_played_well.opposite.pick_stones())

            return TurnOutcome.CAPTURE, self.home_well.num_stones - score_before_play
        return TurnOutcome.NONE, self.home_well.num_stones - score_before_play

    def ask_human_for_play(self):
        to_play = input("Enter the well id to play [0-5]\n")

        while (
                not to_play.isdigit() or                                        # not a number
                int(to_play) > len(self.playable_wells) or int(to_play) < 0 or  # not in range
                not self.playable_wells[int(to_play)].is_playable               # not playable
        ):
            print(f"Invalid input {to_play}. Please enter a valid well id with at least one stone")
            to_play = input("Enter the well id to play [0-5]\n")
        return self.playable_wells[int(to_play)]

    def play(self) -> 'tuple[TurnOutcome, int]':
        """
        Method which picks the best well to play and plays it depending on the algorithm the player follows

        Returns
        -------
        TurnOutcome:
            the outcome of this play
        int:
            the gained stones in the home well
        """
        if self._algorithm == PlayerAlgorithm.MINIMAX:
            choice, _ = self.minimax()
        elif self._algorithm == PlayerAlgorithm.RANDOM:
            choice = random.choice(self.playable_wells)
        elif self._algorithm == PlayerAlgorithm.HUMAN:
            choice = self.ask_human_for_play()
        else:
            raise NotImplementedError("This algorithm isn't implemented yet.")
        if self._algorithm != PlayerAlgorithm.HUMAN and self.verbose:
            print(f"Player {self.name} plays well {choice.position}, which had {choice.num_stones} stones in it.")
        return self.do_play(choice)

    def clone_state(
            self,
            reduce_minimax: bool = True,
            force_algorithm: 'Optional[PlayerAlgorithm]' = None
    ) -> 'tuple(Player, Player)':
        """
        Creates a copy of the current state of the game by returning copies of the two players.

        Parameters
        ----------
        reduce_minimax: bool
            whether to decrement the minimax level for the cloned players.
        force_algorithm: PlayerAlgorithm - optional
            whether to force a given algorithm for the copies. useful for minimax, which assumes both players work with
            the minimax algorithm

        Returns
        -------
        Player:
            a copy of this player.
        Player:
            a copy of the opponent
        """
        me_clone = Player(
                name=self.name,
                minimax_depth=self.minimax_depth - (1 if reduce_minimax else 0),
                init_stones=self.playable_wells_ndarray(),
                init_home=self.home_well.num_stones,
                algorithm=self._algorithm if force_algorithm is None else force_algorithm
            )
        them_clone = Player(
                name=self.opponent.name,
                minimax_depth=self.minimax_depth - (1 if reduce_minimax else 0),
                init_stones=self.opponent.playable_wells_ndarray(),
                init_home=self.opponent.home_well.num_stones,
                algorithm=self.opponent._algorithm if force_algorithm is None else force_algorithm
            )
        me_clone.set_opponent(them_clone)
        them_clone.set_opponent(me_clone)
        return me_clone, them_clone

    def simulate_move(self, move: PlayableWell) -> 'tuple(TurnOutcome, int, Player, Player)':
        """
        Simulates the outcome and score that playing this particular play would generate. The algorithm assumes
        the other player is playing with the minimax algorithm

        Parameters
        ----------
        move: PlayableWell
            the move to simulate
        deeper: bool
            whether or not to reduce the depth in the cloning process of the simulation

        Returns
        -------
        TurnOutcome:
            the outcome of the move
        int:
            the gained stones in the home well from this move
        Player:
            the clone of this player after making the move
        Player:
            the clone of the opponent after this player made the move
        """
        me_clone, them_clone = self.clone_state(reduce_minimax=True, force_algorithm=PlayerAlgorithm.MINIMAX)
        outcome, score = me_clone.do_play(me_clone.playable_wells[move.position])
        # if we have no more stones left, the opponent wins all their stones
        if len(me_clone.available_wells) == 0:
            score -= np.sum(them_clone.playable_wells_ndarray())
        return outcome, score, me_clone, them_clone

    def minimax(self) -> 'tuple(PlayableWell, int)':
        """
        Performs a minimax analysis and returns the best move to play.

        Returns
        -------
        PlayableWell:
            the well to play
        int:
            the estimated score this move will bring
        """
        score_moves = {}
        if len(self.available_wells) == 1:
            return self.available_wells[0], 0
        for move in self.available_wells:
            outcome, score, me_clone, them_clone = self.simulate_move(move)
            if outcome == TurnOutcome.PLAY_AGAIN:
                # if we can play again, we call minimax again on the clone to go as deep as we need.
                if len(me_clone.available_wells) > 0:
                    _, sub_score = me_clone.minimax()
                    score += sub_score
            # now we do the minimax if we haven't reached the bottom
            if self.minimax_depth > 0:
                # if there are available moves...
                if len(them_clone.available_wells) > 0:
                    their_move, their_score = them_clone.minimax()
                    score -= their_score

            if score not in score_moves:
                score_moves[score] = []
            score_moves[score].append(move)
        # return a randomly picked move by max score
        max_score = max(score_moves.keys())
        return random.choice(score_moves[max_score]), max_score

    @property
    def available_wells(self) -> 'list[PlayableWell]':
        return [well for well in self.playable_wells if well.is_playable]

    def playable_wells_ndarray(self) -> np.ndarray:
        """
        Exports this player's playable well's stone count as a numpy array

        Returns
        -------
        np.ndarray:
            the array representation of the playable wells
        """
        return np.array([w.num_stones for w in self.playable_wells]).astype(int)


class Game:
    """
    The game class, which contains two players, and can check when a game is over.
    """
    def __init__(self, player_1: Player, player_2: Player, verbose=True):
        self.player_1 = player_1
        self.player_2 = player_2
        self.player_1.verbose = verbose
        self.player_2.verbose = verbose
        self.player_1.set_opponent(self.player_2)
        self.player_2.set_opponent(self.player_1)
        self.verbose = verbose

        if self.verbose:
            print("Tossing coin to determine first player...")
        self.__next_player = random.choice((self.player_1, self.player_2))
        if self.verbose:
            print(f"Player '{self.__next_player.name}' will go first.")

    def state_to_ndarray(self, player: Player):
        if player.name == self.player_1.name:
            player = self.player_1
        else:
            player = self.player_2
        output = np.full((3, 8), '  ')
        output[0, 1:7] = np.flip(player.opponent.playable_wells_ndarray())
        output[2, 1:7] = player.playable_wells_ndarray()
        output[1, 0] = player.opponent.home_well.num_stones
        output[1, 7] = player.home_well.num_stones
        return output

    def str_for_player(self, player: Player) -> str:
        array = self.state_to_ndarray(player)
        return (
                '   ' + player.opponent.name + '\n' +
                '    ' + ''.join([f'  {5 - x}  ' for x in range(0,6)]) + "\n" +
                ''.join(['-' for _ in range(40)]) + "\n" +
                ''.join([f"( {x} )" if x != '  ' else '  |  ' for x in array[0]]) + "\n" +
                ''.join([f"( {x} )" if x != '  ' else '     ' for x in array[1]]) + "\n" +
                ''.join([f"( {x} )" if x != '  ' else '  |  ' for x in array[2]]) + "\n" +
                ''.join(['-' for _ in range(40)]) + "\n" +
                '    ' + ''.join([f'  {x}  ' for x in range(0, 6)]) + "\n" +
                '   ' + player.name + '\n'
        )

    def next_player(self):
        to_return = self.__next_player
        self.__next_player = self.__next_player.opponent
        return to_return

    @property
    def finished(self) -> bool:
        for player in [self.player_1, self.player_2]:
            if len(player.opponent.available_wells) == 0:
                for well in player.available_wells:
                    player.home_well.add_stones(well.pick_stones())
                return True
        return False

    def run(self):
        while not self.finished:
            player = self.next_player()
            play_again = True
            while play_again and not self.finished:
                if self.verbose:
                    print(self.str_for_player(self.player_1))
                outcome, gain = player.play()
                header = "" if gain < 4 else "What a play! "
                footer = ", with a capture!" if outcome == TurnOutcome.CAPTURE else "."
                play_again = outcome == TurnOutcome.PLAY_AGAIN
                if play_again:
                    footer = footer + f"\n{player.name} plays again..."
                if self.verbose:
                    print(f"{header}{player.name} secured {gain} stones{footer}")

        if self.verbose:
            print(f"{self.player_1.name} score:", self.player_1.home_well.num_stones)
            print(f"{self.player_2.name} score:", self.player_2.home_well.num_stones)