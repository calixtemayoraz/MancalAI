from game.components import Game, TurnOutcome, Player, PlayerAlgorithm


if __name__ == '__main__':
    p1 = Player("Minimax_1", algorithm=PlayerAlgorithm.MINIMAX, minimax_depth=1)
    p2 = Player("Minimax_2", algorithm=PlayerAlgorithm.MINIMAX, minimax_depth=2)
    game = Game(p1, p2, verbose=True)
    game.run()
